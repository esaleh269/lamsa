package com.example.mohga.lamsa.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mohga.lamsa.Fargments.Search_Fragment;
import com.example.mohga.lamsa.R;
import com.example.mohga.lamsa.controller.AllData_Adapter;
import com.example.mohga.lamsa.controller.Brides_adapter;
import com.example.mohga.lamsa.controller.Categories_Recycler2;
import com.example.mohga.lamsa.controller.Face_adapter;
import com.example.mohga.lamsa.controller.Hair_adapter;
import com.example.mohga.lamsa.controller.Lenses_adapter;
import com.example.mohga.lamsa.controller.Makeup_adapter;
import com.example.mohga.lamsa.controller.MySingleTon;
import com.example.mohga.lamsa.model.AllDataModel;
import com.example.mohga.lamsa.model.Categories;
import com.example.mohga.lamsa.model.Categories1;
import com.example.mohga.lamsa.model.URL1;
import com.rom4ek.arcnavigationview.ArcNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Homepage extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,Categories_Recycler2.interfacee ,Brides_adapter.interfacee
        ,Makeup_adapter.interfacee,Face_adapter.interfacee,Hair_adapter.interfacee,Lenses_adapter.interfacee, SearchView.OnQueryTextListener{
    ArrayList<Categories1> Category_list = new ArrayList<>();
    ArrayList<Categories1> Category_list1 = new ArrayList<>();
    ArrayList<Categories1> Category_list2 = new ArrayList<>();
    ArrayList<Categories1> Category_list3 = new ArrayList<>();
    ArrayList<Categories1> Category_list4 = new ArrayList<>();
    ArrayList<Categories> len_list = new ArrayList<>();
    Brides_adapter categoriesRecycler_;
    Categories_Recycler2 categoriesRecycler_2;
    Makeup_adapter categoriesRecycler_3;
    Face_adapter categoriesRecycler_4;
    Hair_adapter categoriesRecycler_5;
    Lenses_adapter lenses_adapter;
    RecyclerView recyclerView2,recyclerView3,recyclerView4,recyclerView5,recyclerView6,recyclerView7;
    RequestQueue requestQueue ,r;
    TextView bride,makeups,faces,hairs;
    Dialog myDialog;
     EditText _name, _email,editTextUsername, editTextPassword,editphone,editTextEmail;
    String username1,name,email,password,mobile,confpass;
    int logid ;
    AlertDialog.Builder builder;
    String url = "http://lamsa.pioneers-solutions.org/Api/Helper/AddUser?";
    ArcNavigationView navigationView;
    DrawerLayout drawer ;
    Toolbar toolbar ;
    TextView textView ;
    RecyclerView recyclerView ;
    ArrayList<AllDataModel> dataa ;
    AllData_Adapter allData_adapter ;
    LinearLayoutManager linearLayoutManager ;
    FrameLayout frameLayout ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    //    frameLayout= (FrameLayout) findViewById(R.id.fram);
        myDialog = new Dialog(this);
        bride=(TextView)findViewById(R.id.brides);
        makeups=(TextView)findViewById(R.id.makeup);
        faces=(TextView)findViewById(R.id.face);
        hairs=(TextView)findViewById(R.id.hair);

        set_cat_req();
        set_cat_req1();
        set_cat_req2();
        set_cat_req3();
        set_cat_req4();
        set_cat_req5();
        ShowAllData();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (ArcNavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
//        showitem();


     //   NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
       // navigationView.setNavigationItemSelectedListener(this);
    }

    public void ShowAllData (){
        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                "http://lamsa.pioneers-solutions.org/Api/Helper/GetAllServices", null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        dataa = new ArrayList<>(response.length());

                        for (int i = 0 ; i< response.length(); i++){
                            try {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                AllDataModel allDataModel= new AllDataModel();
                                allDataModel.setName(jsonObject.getString("ServiceName"));
                                allDataModel.setId(jsonObject.getInt("Id"));
                                String img = jsonObject.getString("UrlImage");
                                img = img.substring(0,2);
                                String img_with_domain  = "http://lamsa.pioneers-solutions.org/"+img ;
                                allDataModel.setImg(jsonObject.getString(img_with_domain));

                                dataa.add(allDataModel);


                            } catch (JSONException e) {
                                Toast.makeText(Homepage.this, ""+e.toString(), Toast.LENGTH_SHORT).show();
                            }

                        }

                        recyclerView = (RecyclerView) findViewById(R.id.alldatarec);
                        allData_adapter = new AllData_Adapter(dataa,Homepage.this);
                        linearLayoutManager  = new LinearLayoutManager(Homepage.this,LinearLayoutManager.VERTICAL,false);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(allData_adapter);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Homepage.this, ""+error.toString(), Toast.LENGTH_SHORT).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    public void set_cat_req () {

        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET , URL1.domain + "Api/Helper/GetCategories", null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject jsonObject = (JSONObject) response.get(i);
                                Categories1 categories = new Categories1();
                                categories.setId(jsonObject.getInt("Id"));
                                categories.setName(jsonObject.getString("Name"));
                                String xx = jsonObject.getString("Image");
                                String xxxx=xx.substring(3,xx.length());
                                String xxx = URL1.domain + xxxx;
                                categories.setImage(xxx);
                                Category_list.add(categories);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        recyclerView2 = (RecyclerView) findViewById(R.id.recycler2);
                        categoriesRecycler_2 = new Categories_Recycler2(Category_list,Homepage.this);
                        LinearLayoutManager layoutManager2 = new LinearLayoutManager(Homepage.this, LinearLayoutManager.HORIZONTAL, false);
                        recyclerView2.setLayoutManager(layoutManager2);
                        categoriesRecycler_2.setlistner(Homepage.this);
                        recyclerView2.setItemAnimator(new DefaultItemAnimator());
                        recyclerView2.setAdapter(categoriesRecycler_2);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Homepage.this, "" + error.toString(), Toast.LENGTH_LONG).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
    public void set_cat_req1 () {

        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL1.domain + "Api/Helper/GetServicesOfCategories?id=1008", null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject jsonObject = (JSONObject) response.get(i);
                                Categories1 categories = new Categories1();
                                categories.setId(jsonObject.getInt("Id"));
                                categories.setName(jsonObject.getString("Name"));
                                String xx = jsonObject.getString("Image");
                                String xxxx=xx.substring(3,xx.length());
                                String xxx = URL1.domain + xxxx;
                                categories.setImage(xxx);
                                Category_list1.add(categories);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                        recyclerView3 = (RecyclerView) findViewById(R.id.recycler3);
                        categoriesRecycler_ = new Brides_adapter(Category_list1,Homepage.this);
                        LinearLayoutManager layoutManager2 = new LinearLayoutManager(Homepage.this, LinearLayoutManager.HORIZONTAL, false);
                        recyclerView3.setLayoutManager(layoutManager2);
                        categoriesRecycler_.setlistner(Homepage.this);
                        recyclerView3.setItemAnimator(new DefaultItemAnimator());
                        recyclerView3.setAdapter(categoriesRecycler_);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Homepage.this, "" + error.toString(), Toast.LENGTH_LONG).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);

    }
    public void set_cat_req2 () {

        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL1.domain + "Api/Helper/GetServicesOfCategories?id=1014", null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject jsonObject = (JSONObject) response.get(i);
                                Categories1 categories = new Categories1();
                                categories.setId(jsonObject.getInt("Id"));
                                categories.setName(jsonObject.getString("Name"));
                                String xx = jsonObject.getString("Image");
                                String xxxx=xx.substring(3,xx.length());
                                String xxx = URL1.domain + xxxx;
                                categories.setImage(xxx);
                                Category_list2.add(categories);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                        recyclerView4 = (RecyclerView) findViewById(R.id.recycler4);
                        categoriesRecycler_3 = new Makeup_adapter(Category_list2,Homepage.this);
                        LinearLayoutManager layoutManager2 = new LinearLayoutManager(Homepage.this, LinearLayoutManager.HORIZONTAL, false);
                        recyclerView4.setLayoutManager(layoutManager2);
                        categoriesRecycler_3.setlistner(Homepage.this);
                        recyclerView4.setItemAnimator(new DefaultItemAnimator());
                        recyclerView4.setAdapter(categoriesRecycler_3);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Homepage.this, "" + error.toString(), Toast.LENGTH_LONG).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
    public void set_cat_req3 () {

        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL1.domain + "Api/Helper/GetServicesOfCategories?id=1015", null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject jsonObject = (JSONObject) response.get(i);
                                Categories1 categories = new Categories1();
                                categories.setId(jsonObject.getInt("Id"));
                                categories.setName(jsonObject.getString("Name"));
                                String xx = jsonObject.getString("Image");
                                String xxxx=xx.substring(3,xx.length());
                                String xxx = URL1.domain + xxxx;
                                categories.setImage(xxx);
                                Category_list3.add(categories);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                        recyclerView5 = (RecyclerView) findViewById(R.id.recycler5);
                        categoriesRecycler_4 = new Face_adapter(Category_list3,Homepage.this);
                        LinearLayoutManager layoutManager2 = new LinearLayoutManager(Homepage.this, LinearLayoutManager.HORIZONTAL, false);
                        recyclerView5.setLayoutManager(layoutManager2);
                        categoriesRecycler_4.setlistner(Homepage.this);
                        recyclerView5.setItemAnimator(new DefaultItemAnimator());
                        recyclerView5.setAdapter(categoriesRecycler_4);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Homepage.this, "" + error.toString(), Toast.LENGTH_LONG).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
    public void set_cat_req4 () {

        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL1.domain + "Api/Helper/GetServicesOfCategories?id=1016", null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject jsonObject = (JSONObject) response.get(i);
                                Categories1 categories = new Categories1();
                                categories.setId(jsonObject.getInt("Id"));
                                categories.setName(jsonObject.getString("Name"));
                                String xx = jsonObject.getString("Image");
                                String xxxx=xx.substring(3,xx.length());
                                String xxx = URL1.domain + xxxx;
                                categories.setImage(xxx);
                                Category_list4.add(categories);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                        recyclerView6 = (RecyclerView) findViewById(R.id.recycler6);
                        categoriesRecycler_5 = new Hair_adapter(Category_list4,Homepage.this);
                        LinearLayoutManager layoutManager2 = new LinearLayoutManager(Homepage.this, LinearLayoutManager.HORIZONTAL, false);
                        recyclerView6.setLayoutManager(layoutManager2);
                        categoriesRecycler_5.setlistner(Homepage.this);
                        recyclerView6.setItemAnimator(new DefaultItemAnimator());
                        recyclerView6.setAdapter(categoriesRecycler_5);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Homepage.this, "" + error.toString(), Toast.LENGTH_LONG).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
    public void set_cat_req5 () {

        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL1.domain + "Api/Helper/GetAllLences", null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject jsonObject = (JSONObject) response.get(i);
                                Categories categories = new Categories();
                                categories.setId(jsonObject.getInt("Id"));
                                categories.setName(jsonObject.getString("Name"));
//                                String xx = jsonObject.getString("Image");
//                                String xxxx=xx.substring(3,xx.length());
//                                String xxx = URL1.domain + xxxx;
//                                categories.setImage(xxx);
                                len_list.add(categories);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                        recyclerView7 = (RecyclerView) findViewById(R.id.recycler7);
                        lenses_adapter = new Lenses_adapter(len_list,Homepage.this);
                        LinearLayoutManager layoutManager2 = new LinearLayoutManager(Homepage.this, LinearLayoutManager.HORIZONTAL, false);
                        recyclerView7.setLayoutManager(layoutManager2);
                        lenses_adapter.setlistner(Homepage.this);
                        recyclerView7.setItemAnimator(new DefaultItemAnimator());
                        recyclerView7.setAdapter(lenses_adapter);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Homepage.this, "" + error.toString(), Toast.LENGTH_LONG).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }




    @Override
    public void click(int pos) {
        Intent intent=new Intent(this,Brides_cat.class);
        Bundle bundle = new Bundle();
        bundle.putInt("index",Category_list.get(pos).getId());
        bundle.putString("title",Category_list.get(pos).getName());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void click1(int pos) {
        Intent intent=new Intent(this,Details.class);
        Bundle bundle = new Bundle();
        bundle.putInt("index",Category_list1.get(pos).getId());
        bundle.putString("title",bride.getText().toString());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void click2(int pos) {
        Intent intent=new Intent(this,Details.class);
        Bundle bundle = new Bundle();
        bundle.putInt("index",Category_list2.get(pos).getId());
        bundle.putString("title",makeups.getText().toString());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void click3(int pos) {
        Intent intent=new Intent(this,Details.class);
        Bundle bundle = new Bundle();
        bundle.putInt("index",Category_list3.get(pos).getId());
        bundle.putString("title",faces.getText().toString());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void click4(int pos) {
        Intent intent=new Intent(this,Details.class);
        Bundle bundle = new Bundle();
        bundle.putInt("index",Category_list4.get(pos).getId());
        bundle.putString("title",hairs.getText().toString());
        intent.putExtras(bundle);
        startActivity(intent);

    }

    @Override
    public void click5(int pos) {
        Intent intent=new Intent(this,Lenses_details.class);
        Bundle bundle = new Bundle();
        bundle.putInt("index",len_list.get(pos).getId());
        bundle.putInt("position",pos);
        intent.putExtras(bundle);
        startActivity(intent);
    }
    public void Showsignuppop() {
        TextView txtclose;
        Button signup;
        myDialog.setContentView(R.layout.custompopup);
        txtclose = myDialog.findViewById(R.id.back);
        editTextUsername = myDialog.findViewById(R.id.user);
        editTextPassword = myDialog.findViewById(R.id.pass);
        editphone = myDialog.findViewById(R.id.phone);
        editTextEmail = myDialog.findViewById(R.id.email);
        signup = myDialog.findViewById(R.id.btnfollow);
        txtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
                Intent intent=new Intent(getBaseContext(),Homepage.class);
                startActivity(intent);
                logaftersignup();
            }});
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }
    public void Showloginpop() {
        TextView txtclose;
        Button log;
        myDialog.setContentView(R.layout.custompopuplogin);
        txtclose = myDialog.findViewById(R.id.back);
        _name = myDialog.findViewById(R.id.name);
        _email = myDialog.findViewById(R.id.pass);
        log = myDialog.findViewById(R.id.btn);
        txtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              log();

            }});

        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }

    //noinspection SimplifiableIfStatemen
    public void log(){
    requestQueue = Volley.newRequestQueue(getBaseContext());

    JSONObject jsonObject = new JSONObject();
    try {
        jsonObject.put("email",_name.getText().toString());
        jsonObject.put("password",_email.getText().toString());
    } catch (JSONException e) {
        e.printStackTrace();
    }


    JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(Request.Method.POST, "http://lamsa.pioneers-solutions.org/Api/Helper/SignIn?"+"&Username="+

            _name.getText().toString()+
            "&Password="+_email.getText().toString()
,null,


            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    for (int i = 0; i < response.length(); i++) {
                        try {

                            JSONObject jsonObject = (JSONObject) response.get(i);
                             String userid = jsonObject.getString("Id");
                             logid=Integer.parseInt(userid);
Toast.makeText(getBaseContext(),"successfully loging",Toast.LENGTH_LONG).show();
                            Intent intent3 = new Intent(Homepage.this,Homepage.class);
                            startActivity(intent3);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(Homepage.this, "" + e.toString(), Toast.LENGTH_LONG).show();
                        }
                        SharedPreferences pref = getSharedPreferences("token",MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = pref.edit();
                        editor1.putInt("userid", logid);
                        editor1.putString("name",_name.getText().toString());
                        editor1.apply();
                    }}
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    Toast.makeText(Homepage.this, ""+error.toString(), Toast.LENGTH_LONG).show();

                }
            }

    );

    requestQueue.add(jsonObjectRequest);

}

    private void registerUser() {
        username1 =editTextUsername.getText().toString().trim();
        name = editTextUsername.getText().toString().trim();
        email = editTextEmail.getText().toString().trim();
        password=editTextPassword.getText().toString().trim();
        mobile = editphone.getText().toString().trim();

        confpass = editTextPassword.getText().toString().trim();

        if (username1.equals("")||email.equals("")||password.equals("")||mobile.equals("")||confpass.equals("")||name.equals(""))
        {
            builder.setTitle("Some Thing Went Wrong .......");
            builder.setMessage("Please Fill All The Fields.......");

            displayAlert("input_error");
        }

        else
        {
            if (!(password.equals(confpass)))
            {
                builder.setTitle("Some Thing Went Wrong .......");
                builder.setMessage("Your passwords are not matcheing.......");
                displayAlert("input_error");


            }

            else
            {
                StringRequest stringRequest = new StringRequest(Request.Method.POST,

                        url+"&Username="+username1+"&Password="+password+"&Mobile="+mobile+"&Email="+email+"&Name="+name, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        Toast.makeText(Homepage.this, "success"+response, Toast.LENGTH_SHORT).show();
                        SharedPreferences pref = getSharedPreferences("signup",MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = pref.edit();
                        editor1.putString("name",editTextUsername.getText().toString());
                        editor1.putString("pass",editTextPassword.getText().toString());
                        editor1.apply();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(Homepage.this, "error"+error.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String,String>  params = new HashMap<String, String>();
                        params.put("Username",username1);
                        params.put("Name",name);
                        params.put("Password",password);
                        params.put("Mobile",mobile);
                        params.put("Email",email);

                        return params;
                    }
                };

                MySingleTon.getInstant(Homepage.this).addtoRequest(stringRequest);
            }
        }

        }

    public void displayAlert(final String Code)
        {
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
@Override
public void onClick(DialogInterface dialog, int which) {

        if (Code.equals("input_error"))
        {
        editTextPassword.setText("");
      editTextPassword.setText("");
        }

        }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        }
    public void logaftersignup(){
        r= Volley.newRequestQueue(getBaseContext());
        SharedPreferences preferences = getSharedPreferences("signup", MODE_PRIVATE);
        final String nam = preferences.getString("name", "");
        String pas = preferences.getString("pass", "");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email",nam);
            jsonObject.put("password",pas);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(Request.Method.POST, "http://lamsa.pioneers-solutions.org/Api/Helper/SignIn?"+"&Username="+

                nam+
                "&Password="+pas
                ,null,


                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject jsonObject = (JSONObject) response.get(i);
                                String userid = jsonObject.getString("Id");
                                logid=Integer.parseInt(userid);
                                Toast.makeText(getBaseContext(),"successfully loging",Toast.LENGTH_LONG).show();
                                Intent intent3 = new Intent(Homepage.this,Homepage.class);
                                startActivity(intent3);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(Homepage.this, "" + e.toString(), Toast.LENGTH_LONG).show();
                            }
                            SharedPreferences pref = getSharedPreferences("token",MODE_PRIVATE);
                            SharedPreferences.Editor editor1 = pref.edit();
                            editor1.putInt("userid", logid);
                            editor1.putString("name",nam);
                            editor1.apply();
                        }}
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Toast.makeText(Homepage.this, ""+error.toString(), Toast.LENGTH_LONG).show();

                    }
                }

        );

        r.add(jsonObjectRequest);

    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.log_in_icon:
                Showloginpop();
                drawer.closeDrawers();
                break;
            case R.id.log_out_icon:
            SharedPreferences pref = getSharedPreferences("token",MODE_PRIVATE);
            SharedPreferences.Editor editor1 = pref.edit() ;
            editor1.putInt("userid",0);
            editor1.apply();
            Intent intent3 = new Intent(Homepage.this,MainActivity.class);
            startActivity(intent3);
            drawer.closeDrawers();
            break;

            case R.id.sigh_up_icon:
                Showsignuppop();
                drawer.closeDrawers();
                break;
            case R.id.setting_icon:
                drawer.closeDrawers();
                break;
            case R.id.fav_icon:
                Intent intent=new Intent(this,Favourite.class);
                startActivity(intent);
                drawer.closeDrawers();
                break;





        }
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.searching);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Homepage.this, "searching", Toast.LENGTH_SHORT).show();
            }
        });
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.searching:
//                Intent intent = new Intent(Homepage.this,Favourite.class);
//                startActivity(intent);
//                android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
//                android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
//                transaction.replace(R.id.fram,new Search_Fragment(),"");
//                transaction.commit();



//                if (frameLayout.getVisibility()==View.VISIBLE) {
//
//                    recyclerView.setVisibility(View.VISIBLE);
//                    frameLayout.setVisibility(View.INVISIBLE);
//
//                }else {
//                    frameLayout.setVisibility(View.VISIBLE);
//                    recyclerView.setVisibility(View.INVISIBLE);
//
//                 }

                break;

        }
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}


//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
////        if (id == R.id.b7) {
////            Showsignuppop();
////        }
////        // Handle the camera action
////        else if (id == R.id.b2) {
////            Showloginpop();
////        }
////        else if (id == R.id.b3) {
////            Intent intent=new Intent(this,Userdetails.class);
////            startActivity(intent);
////        }
////
////        else if (id == R.id.b5) {
////            Intent intent=new Intent(this,Favourite.class);
////            startActivity(intent);
////        }
////        else if (id == R.id.b4) {
////            item.setVisible(false);
////
////        }
////        else if (id == R.id.b6) {
////            SharedPreferences pref = getSharedPreferences("token",MODE_PRIVATE);
////            SharedPreferences.Editor editor1 = pref.edit() ;
////            editor1.putInt("userid",0);
////            editor1.apply();
////            Intent intent3 = new Intent(Homepage.this,MainActivity.class);
////            startActivity(intent3);
////        }
////
////
////        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
////        drawer.closeDrawer(GravityCompat.START);
////        return true;
////    }
///////////////control items in navigation menu
////    private void hideItemnotlog()
////    {
////        navigationView = (NavigationView) findViewById(R.id.nav_view);
////        navigationView.setNavigationItemSelectedListener(this);
////        Menu nav_Menu = navigationView.getMenu();
//////        nav_Menu.findItem(R.id.b1).setVisible(false);
//////        nav_Menu.findItem(R.id.b3).setVisible(false);
//////        nav_Menu.findItem(R.id.b4).setVisible(false);
//////        nav_Menu.findItem(R.id.b5).setVisible(false);
//////        nav_Menu.findItem(R.id.b6).setVisible(false);
////    }
////    private void hideItemnlog()
////    {
////        navigationView = (NavigationView) findViewById(R.id.nav_view);
////        navigationView.setNavigationItemSelectedListener(this);
////        Menu nav_Menu = navigationView.getMenu();
////        nav_Menu.findItem(R.id.b2).setVisible(false);
////        nav_Menu.findItem(R.id.b7).setVisible(false);
////        SharedPreferences preferences = getSharedPreferences("token", MODE_PRIVATE);
////       String n = preferences.getString("name", "");
////       ///////change color of menu item
////        SpannableString s = new SpannableString(n);
////        s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), 0, s.length(), 0);
////        s.setSpan(new RelativeSizeSpan(1.5f), 0, n.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
////        nav_Menu.findItem(R.id.b1).setTitle(s);
////    }
////    public void showitem(){
////        SharedPreferences preferences = getSharedPreferences("token", MODE_PRIVATE);
////        int userId = preferences.getInt("userid", 0);
////        Toast.makeText(getBaseContext(),"id"+userId,Toast.LENGTH_LONG).show();
////        if (userId==0){
////            hideItemnotlog();
////        }
////        else {
////            hideItemnlog();
////        }
////    }
//
