package com.example.mohga.lamsa.controller;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mohga.lamsa.R;
import com.example.mohga.lamsa.model.AllDataModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;



public class AllData_Adapter extends RecyclerView.Adapter<AllData_Adapter.VHolder> {

    private ArrayList<AllDataModel> Data ;
    Context context ;
    private Service_Click service_click ;

    public AllData_Adapter (ArrayList<AllDataModel> list , Context ccontext){

        this.Data = list ;
        this.context = ccontext ;
     }

    public void SetListner (Service_Click service_click1){
        service_click = service_click1;
    }


    @Override
    public VHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.alldataadapter,parent,false);
        return new AllData_Adapter.VHolder(view);
    }

    @Override
    public void onBindViewHolder(VHolder holder, int position) {

        AllDataModel allDataModel = Data.get(position);
        holder.name.setText(allDataModel.getName());
        Picasso.with(context).load(allDataModel.getImg()).into(holder.imageView);
     }

    @Override
    public int getItemCount() {
        return Data.size();
    }

    public class VHolder extends RecyclerView.ViewHolder{

        TextView name ;
        ImageView imageView ;
        CardView cardView ;

        public VHolder(View itemView) {
            super(itemView);
            imageView =itemView.findViewById(R.id.allimg);
            name =itemView.findViewById(R.id.alltxt);
            cardView =itemView.findViewById(R.id.allcard);

        }
    }

    public interface Service_Click {
        void OnServiceClick (ArrayList <AllDataModel> arrayList);
    }
}
