package com.example.mohga.lamsa.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.mohga.lamsa.R;
import com.example.mohga.lamsa.controller.Categories_Recycler1;
import com.example.mohga.lamsa.model.Categories;
import com.example.mohga.lamsa.model.URL1;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Brides_cat extends AppCompatActivity implements Categories_Recycler1.interfacee{
    ArrayList<Categories> Category_list = new ArrayList<>();
    Categories_Recycler1 categoriesRecycler_;
    RecyclerView recyclerView2;
    RequestQueue requestQueue ;
    TextView title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brides_cat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        title=(TextView)findViewById(R.id.toolbar_title);
        Bundle b = getIntent().getExtras();
        String titles = b.getString("title");
        title.setText(titles);
        set_cat_req();
    }
    public void set_cat_req () {
        Bundle b = getIntent().getExtras();
        int index = b.getInt("index");
        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL1.domain + "Api/Helper/GetServicesOfCategories?id="+index, null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject jsonObject = (JSONObject) response.get(i);
                                Categories categories = new Categories();
                                categories.setId(jsonObject.getInt("Id"));
                                categories.setName(jsonObject.getString("Name"));
                                String xx = jsonObject.getString("Image");
                                String xxxx=xx.substring(3,xx.length());
                                String xxx = URL1.domain + xxxx;
                                categories.setImg(xxx);
                                Category_list.add(categories);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        recyclerView2 = (RecyclerView) findViewById(R.id.recycler2);
                        categoriesRecycler_ = new Categories_Recycler1(Category_list, Brides_cat.this);
                        LinearLayoutManager layoutManager2 = new LinearLayoutManager(Brides_cat.this, LinearLayoutManager.VERTICAL, false);
                        recyclerView2.setLayoutManager(layoutManager2);
                        categoriesRecycler_.setlistner(Brides_cat.this);
                        recyclerView2.setItemAnimator(new DefaultItemAnimator());
                        recyclerView2.setAdapter(categoriesRecycler_);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Brides_cat.this, "" + error.toString(), Toast.LENGTH_LONG).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.homepage, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.search) {
            return true;
        }
        if (android.R.id.home==item.getItemId()) {
            Intent i=new Intent(this,Homepage.class);
            startActivity(i);
            return true;
        } return super.onOptionsItemSelected(item);}


    @Override
    public void click(int pos) {
        Intent intent=new Intent(this,Details.class);
        Bundle bundle = new Bundle();
        bundle.putInt("index",Category_list.get(pos).getId());
        bundle.putString("title",title.getText().toString());
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
