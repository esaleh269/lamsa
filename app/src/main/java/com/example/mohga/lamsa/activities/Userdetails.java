package com.example.mohga.lamsa.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.mohga.lamsa.R;
import com.example.mohga.lamsa.model.URL1;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Userdetails extends AppCompatActivity {
    TextView name, pass, mobile, email;
RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userdetails);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        name=(TextView)findViewById(R.id.cont_name);
        pass=(TextView)findViewById(R.id.password);
        email=(TextView)findViewById(R.id.ema);
        mobile = (TextView) findViewById(R.id.mobi);
getdetails();
    }
    public void getdetails(){
        requestQueue = Volley.newRequestQueue(getBaseContext());
        SharedPreferences preferences = getSharedPreferences("token", MODE_PRIVATE);
        int userId = preferences.getInt("userid", 0);
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(Request.Method.POST, URL1.domain+"Api/Helper/ShowUser?"
                +"Id="+userId
                ,null,


                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject jsonObject = (JSONObject) response.get(i);
                              name.setText(jsonObject.getString("Username"));
                               pass.setText(jsonObject.getString("Password"));
                               email.setText(jsonObject.getString("Email"));
                                mobile.setText(jsonObject.getString("Mobile"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(Userdetails.this, "" + e.toString(), Toast.LENGTH_LONG).show();
                            }
                        }}
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Toast.makeText(Userdetails.this, ""+error.toString(), Toast.LENGTH_LONG).show();

                    }
                }

        );

        requestQueue.add(jsonObjectRequest);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (android.R.id.home==item.getItemId()) {
            Intent i=new Intent(this,Homepage.class);
            startActivity(i);
            return true;
        } return super.onOptionsItemSelected(item);}


}
