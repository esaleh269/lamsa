package com.example.mohga.lamsa.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mohga.lamsa.R;

public class MainActivity extends AppCompatActivity {
    TextView textView;
    LinearLayout l1;
    Animation translate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView=(TextView)findViewById(R.id.t);
        l1 = (LinearLayout) findViewById(R.id.l1);
        translate= AnimationUtils.loadAnimation(getBaseContext(), R.anim.anim_translate);
        l1.setAnimation(translate);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getBaseContext(),Homepage.class);
                startActivity(i);

            }
        });

    }
}
