package com.example.mohga.lamsa.controller;

/**
 * Created by SoftCo on 11/18/2017.
 */


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mohga.lamsa.R;
import com.example.mohga.lamsa.model.URL1;
import com.example.mohga.lamsa.model.favCategories;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class Favouriteadapter extends RecyclerView.Adapter<Favouriteadapter.MyHolderr> {
interfacee interfacee;
    int favid;
    String favtype;
    private ArrayList<favCategories> Category_list;
    private Context context;

    public Favouriteadapter(ArrayList<favCategories> list, Context c) {
        this.Category_list=list;
        this.context=c;

    }

    @Override
    public MyHolderr onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_recycleshape, parent, false);

        return  new MyHolderr(itemView);
    }
    public void setlistner(interfacee interfaceee){
        this.interfacee=interfaceee;
    }

    @Override
    public void onBindViewHolder(final MyHolderr holder, final int position) {

        final favCategories categories = Category_list.get(position);
        holder.cat_title.setText(categories.getName());

        holder.deletefav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favid = Category_list.get(position).getId();
                favtype = Category_list.get(position).getType();
                deletefavourites();
            }
        });
        Picasso.with(context).load(categories.getImg()).resize(500, 500).into(holder.cat_pic);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfacee.click(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return Category_list.size();
    }

    public void deletefavourites() {
        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL1.domain + "api/Helper/DeleteFaveorite/" + favid + "/" + favtype,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        Toast.makeText(context, response.substring(13, 17), Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Don't work", Toast.LENGTH_LONG).show();
            }
        }) {
            //adding parameters to the request

        };
        // Add the request to the RequestQueue.
        requestQueue.add(stringRequest);
    }

    public interface interfacee {
        void click(int pos);
    }

    public class MyHolderr extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView cat_pic, deletefav;
        TextView cat_title ;
        CardView cat_card ;

        public MyHolderr(View itemView) {
            super(itemView);
            cat_pic = itemView.findViewById(R.id.rec2_img);
            deletefav = itemView.findViewById(R.id.del);
            cat_title = itemView.findViewById(R.id.cat_title);
            cat_card = itemView.findViewById(R.id.cat_card);
        }

        @Override
        public void onClick(View v) {
        }
    }
}

