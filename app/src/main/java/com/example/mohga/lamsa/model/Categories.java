package com.example.mohga.lamsa.model;

/**
 * Created by SoftCo on 11/18/2017.
 */


public class Categories {

    private int id ;
    private String Description ;
    private String Name ;
    private String img ;
    private String price ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setImg(String img) {
        this.img = img;
    }
}

