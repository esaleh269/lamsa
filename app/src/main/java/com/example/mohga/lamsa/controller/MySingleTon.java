package com.example.mohga.lamsa.controller;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by dell- on 24/01/2018.
 */

public class MySingleTon {

    Context context;

    RequestQueue requestQueue;

    private  static MySingleTon mySingleTon;

    public MySingleTon(Context context) {
        this.context = context;

        requestQueue=getRequestQue();

    }

    private RequestQueue getRequestQue() {

        if (requestQueue==null){

            requestQueue= Volley.newRequestQueue(context);
        }
        return requestQueue;
    }


    public static synchronized MySingleTon getInstant(Context context){

        if (mySingleTon==null){

            mySingleTon= new MySingleTon(context);

        }
        return mySingleTon;

    }


    public <T> void addtoRequest (Request<T> request){

        requestQueue.add(request);

    }

}
