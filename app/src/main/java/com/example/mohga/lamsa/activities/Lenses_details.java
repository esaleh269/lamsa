package com.example.mohga.lamsa.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mohga.lamsa.R;
import com.example.mohga.lamsa.model.Categories;
import com.example.mohga.lamsa.model.URL1;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Lenses_details extends AppCompatActivity {
    TextView Name,desc,price;
    RequestQueue requestQueue ;
    String name="";
    String name1 = "";
    String prices="";
    String description="";
    ImageView imageView, favourite, share;
    Button booking;
    int lenid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lenses_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        SharedPreferences preferences = getSharedPreferences("token", MODE_PRIVATE);
        final int userId = preferences.getInt("userid", 0);
        Name=(TextView)findViewById(R.id.title);
        desc=(TextView)findViewById(R.id.description);
        price=(TextView)findViewById(R.id.pri);
        imageView=(ImageView)findViewById(R.id.rec2_img);
        favourite = (ImageView) findViewById(R.id.fav);
        share = (ImageView) findViewById(R.id.share);
        booking=(Button)findViewById(R.id.booking);
        booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userId==0){
                    Toast.makeText(getBaseContext(),"you must log in",Toast.LENGTH_LONG).show();
                }
                else{
                    booklenses();
                }
            }
        });
        favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userId == 0) {
                    Toast.makeText(getBaseContext(), "you must log in", Toast.LENGTH_LONG).show();
                } else {
                    addtofavourites();
                    favourite.setVisibility(View.INVISIBLE);
                }

            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareIt();
            }
        });
        set_cat_req();
        getfavourite();
    }
    public void set_cat_req () {
        Bundle b = getIntent().getExtras();
        final int index = b.getInt("index");
        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL1.domain + "Api/Helper/GetLences?Id="+index, null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject jsonObject = (JSONObject) response.get(i);
                                String id=jsonObject.getString("Id");
                                lenid=Integer.parseInt(id);
                                String xx = jsonObject.getString("Image");
                                String xxxx=xx.substring(3,xx.length());
                                String xxx = URL1.domain + xxxx;
                                name=jsonObject.getString("Name");
                                Name.setText(name);
description=jsonObject.getString("Descriptione");
                                desc.setText(description);
                                prices=jsonObject.getString("Price");
                                price.setText(prices);
                                Picasso.with(getBaseContext()).load(xxx).into(imageView);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Lenses_details.this, "" + error.toString(), Toast.LENGTH_LONG).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.homepage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.search) {
            return true;
        }
        if (android.R.id.home==item.getItemId()) {
            Intent i=new Intent(this,Homepage.class);
            startActivity(i);
            return true;
        } return super.onOptionsItemSelected(item);}

    public void addtofavourites() {
        requestQueue = Volley.newRequestQueue(getBaseContext());
        SharedPreferences preferences = getSharedPreferences("token", MODE_PRIVATE);
        int userId = preferences.getInt("userid", 0);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL1.domain + "Api/Helper/AddFaveorite?" + "UserId=" + userId + "&ServiceId=" +
                lenid + "&type=len",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        Snackbar snackbar = Snackbar
                                .make(findViewById(R.id.det), "Thanks", Snackbar.LENGTH_LONG);

                        View sbView = snackbar.getView();
                        sbView.setBackgroundColor(getResources().getColor(R.color.white));
                        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(getResources().getColor(R.color.darkbrown));
                        snackbar.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar snackbar = Snackbar
                        .make(findViewById(R.id.det), "didnot work", Snackbar.LENGTH_LONG);

                View sbView = snackbar.getView();
                sbView.setBackgroundColor(getResources().getColor(R.color.darkbrown));
                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
            }
        }) {
            //adding parameters to the request

        };
        // Add the request to the RequestQueue.
        requestQueue.add(stringRequest);
    }

    public void getfavourite() {
        requestQueue = Volley.newRequestQueue(this);
        SharedPreferences preferences = getSharedPreferences("token", MODE_PRIVATE);
        int userId = preferences.getInt("userid", 0);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL1.domain + "Api/Helper/GetFavorite?UserId=" + userId, null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject jsonObject = (JSONObject) response.get(i);
                                Categories categories = new Categories();
                                categories.setName(jsonObject.getString("Name"));
                                name1 = jsonObject.getString("Name");
                                ///if it chose already i cannot choose it again
                                if (name1.equals(name)) {
                                    favourite.setVisibility(View.INVISIBLE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Lenses_details.this, "" + error.toString(), Toast.LENGTH_LONG).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }

    public void booklenses() {

        requestQueue = Volley.newRequestQueue(this);
        SharedPreferences preferences = getSharedPreferences("token", MODE_PRIVATE);
        int userId = preferences.getInt("userid", 0);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL1.domain + "Api/Helper/RequestLenses?" + "UserId=" + userId + "&Lenses=" + lenid,

                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String msg = response.getString("Response");
                            Toast.makeText(Lenses_details.this, "" + msg, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(Lenses_details.this, "" + e.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Toast.makeText(Lenses_details.this, "" + error.toString(), Toast.LENGTH_LONG).show();

                    }
                }

        );

        requestQueue.add(jsonObjectRequest);


    }

    private void shareIt() {
//sharing implementation here
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Lamsa services");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "services are //////");
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }
}
