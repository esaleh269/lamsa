package com.example.mohga.lamsa.Fargments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.mohga.lamsa.R;
import com.example.mohga.lamsa.controller.AllData_Adapter;
import com.example.mohga.lamsa.model.AllDataModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Administrator on 2/14/2018.
 */

public class Search_Fragment extends Fragment {

    TextView textView ;
    RequestQueue requestQueue ;
    RecyclerView recyclerView ;
    ArrayList<AllDataModel> dataa ;
    AllData_Adapter allData_adapter ;
    LinearLayoutManager linearLayoutManager ;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

         View view =  inflater.inflate(R.layout.fragmentlayout,container,false);
         setdata(view);



         return  view ;



    }

    public void setdata (final View view){

        requestQueue = Volley.newRequestQueue(getContext());
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                "http://lamsa.pioneers-solutions.org/Api/Helper/GetAllServices", null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                       dataa = new ArrayList<>(response.length());

                        for (int i = 0 ; i< response.length(); i++){
                            try {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                AllDataModel allDataModel= new AllDataModel();
                                allDataModel.setName(jsonObject.getString("ServiceName"));
                                allDataModel.setId(jsonObject.getInt("Id"));
                                String img = jsonObject.getString("UrlImage");
                                img = img.substring(0,2);
                                String img_with_domain  = "http://lamsa.pioneers-solutions.org/"+img ;
                                allDataModel.setImg(jsonObject.getString(img_with_domain));

                                dataa.add(allDataModel);


                            } catch (JSONException e) {
                                Toast.makeText(view.getContext(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                            }

                        }

                        allData_adapter = new AllData_Adapter(dataa,getContext());
                        recyclerView = view.findViewById(R.id.alldatarec);
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext(),LinearLayoutManager.VERTICAL,false);
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(allData_adapter);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(view.getContext(), ""+error.toString(), Toast.LENGTH_SHORT).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);


    }
}
