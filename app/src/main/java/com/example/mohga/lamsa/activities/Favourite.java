package com.example.mohga.lamsa.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.mohga.lamsa.R;
import com.example.mohga.lamsa.controller.Favouriteadapter;
import com.example.mohga.lamsa.model.URL1;
import com.example.mohga.lamsa.model.favCategories;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Favourite extends AppCompatActivity implements Favouriteadapter.interfacee{
    ArrayList<favCategories> Category_list = new ArrayList<>();
    Favouriteadapter categoriesRecycler_;
    RecyclerView recyclerView2;
    RequestQueue requestQueue ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

       getfavourite();
    }
    public void getfavourite () {
        requestQueue = Volley.newRequestQueue(this);
        SharedPreferences preferences = getSharedPreferences("token", MODE_PRIVATE);
        int userId = preferences.getInt("userid", 0);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL1.domain + "Api/Helper/GetFavorite?UserId="+userId, null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject jsonObject = (JSONObject) response.get(i);
                                favCategories categories = new favCategories();
                                categories.setId(jsonObject.getInt("Id"));
                                categories.setType(jsonObject.getString("Type"));
                                categories.setName(jsonObject.getString("Name"));
                                String xx = jsonObject.getString("Image");
                                String xxxx=xx.substring(3,xx.length());
                                String xxx = URL1.domain + xxxx;
                                categories.setImg(xxx);
                                Category_list.add(categories);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }


                        recyclerView2 = (RecyclerView) findViewById(R.id.recycler2);
                        categoriesRecycler_ = new Favouriteadapter(Category_list, Favourite.this);
                        LinearLayoutManager layoutManager2 = new LinearLayoutManager(Favourite.this, LinearLayoutManager.VERTICAL, false);
                        recyclerView2.setLayoutManager(layoutManager2);
                        categoriesRecycler_.setlistner(Favourite.this);
                        recyclerView2.setItemAnimator(new DefaultItemAnimator());
                        recyclerView2.setAdapter(categoriesRecycler_);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Favourite.this, "" + error.toString(), Toast.LENGTH_LONG).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (android.R.id.home==item.getItemId()) {
            Intent i=new Intent(this,Homepage.class);
            startActivity(i);
            return true;
        } return super.onOptionsItemSelected(item);}

    @Override
    public void click(int pos) {

    }

}
