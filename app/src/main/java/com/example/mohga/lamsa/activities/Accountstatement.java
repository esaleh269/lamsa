package com.example.mohga.lamsa.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mohga.lamsa.R;
import com.example.mohga.lamsa.model.URL1;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Accountstatement extends AppCompatActivity {
    RequestQueue requestQueue;
    JSONObject jsonObject;
    TextView keenum, receiptnum, date, credit, depit, note, type;
    TextView madeen, credittotal, balance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accountstatement);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        keenum = (TextView) findViewById(R.id.textView13);
        receiptnum = (TextView) findViewById(R.id.textView14);
        date = (TextView) findViewById(R.id.textView15);
        credit = (TextView) findViewById(R.id.textView16);
        depit = (TextView) findViewById(R.id.textView17);
        note = (TextView) findViewById(R.id.textView18);
        type = (TextView) findViewById(R.id.textView19);
        madeen = (TextView) findViewById(R.id.textView48);
        credittotal = (TextView) findViewById(R.id.textView42);
        balance = (TextView) findViewById(R.id.textView5);
        GetAccountstatement();
    }

    public void GetAccountstatement() {
        requestQueue = Volley.newRequestQueue(this);
        SharedPreferences preferences = getSharedPreferences("token", MODE_PRIVATE);
        int userId = preferences.getInt("userid", 0);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                URL1.domain + "Api/Helper/GetClientQued/" + userId, null,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            madeen.setText(response.getString("TotalQuedAmountD"));
                            credittotal.setText(response.getString("TotalQuedAmountC"));
                            balance.setText(response.getString("Total"));
                            JSONArray jsonArray = response.getJSONArray("QuedApi");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject object = (JSONObject) jsonArray.get(0);
                                keenum.setText(object.getString("QuedNum"));
                                receiptnum.setText(object.getString("QuedSanadNum"));
                                date.setText(object.getString("QuedPostDate"));
                                credit.setText(object.getString("QuedAmountC"));
                                depit.setText(object.getString("QuedAmountD"));
                                note.setText(object.getString("QuedDiscr"));
                                type.setText(object.getString("QuedSanadKind"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(Accountstatement.this, "" + e.toString(), Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Accountstatement.this, "" + error.toString(), Toast.LENGTH_LONG).show();

                    }
                }

        );
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (android.R.id.home == item.getItemId()) {
            Intent i = new Intent(this, Homepage.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
