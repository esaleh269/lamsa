package com.example.mohga.lamsa.controller;



import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mohga.lamsa.model.Categories1;
import com.example.mohga.lamsa.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class Categories_Recycler2 extends RecyclerView.Adapter<Categories_Recycler2.MyHolderr> {
    private ArrayList<Categories1> Category_list;
    private Context context ;

interfacee interfacee;

    public Categories_Recycler2(ArrayList<Categories1> list, Context c){
        this.Category_list=list;
        this.context=c;

    }

    @Override
    public MyHolderr onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.getcategories, parent, false);

        return  new MyHolderr(itemView);
    }
public void setlistner(interfacee interfaceee){
    this.interfacee=interfaceee;
}
    @Override
    public void onBindViewHolder(final MyHolderr holder, final int position) {

        final Categories1 categories = Category_list.get(position);

        holder.cat_title.setText(categories.getName());
        Picasso.with(context).load(categories.getImage()).resize(500,500).into(holder.cat_pic);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfacee.click(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return Category_list.size();
    }

    public class MyHolderr extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView cat_pic ;
        TextView cat_title ;
        CardView cat_card ;

        public MyHolderr(View itemView) {
            super(itemView);
            cat_pic = (ImageView) itemView.findViewById(R.id.rec2_img);
            cat_title = (TextView) itemView.findViewById(R.id.cat_title);
            cat_card = (CardView) itemView.findViewById(R.id.cat_card);
        }

        @Override
        public void onClick(View v) {

        }
    }
   public interface interfacee{
       void click(int pos);
   }

}

